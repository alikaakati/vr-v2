﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class dialogueManager : MonoBehaviour
{

    public Text dialogueText;


    public Queue<string> sentences;
    void Start()
    {
    }

    public void Start(Dialogue a)
    {
        sentences = new Queue<string>();
        Debug.Log("Start");

        sentences.Clear();
        Debug.Log("Start2");
        foreach (string sentence in a.sentences)
        {
            sentences.Enqueue(sentence);
            Debug.Log("sentence added");

        }


        DisplayNextSentence();
    }
    public void DisplayNextSentence()
    {


        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }

        string sentence = sentences.Dequeue();
        dialogueText.text = sentence;



    }


    public void EndDialogue()
    {
        Debug.Log("End");
    }




}