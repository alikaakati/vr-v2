﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agent : MonoBehaviour
{

    public Dialogue dialogue;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            FindObjectOfType<dialogueManager>().DisplayNextSentence();
        }

    }


    private void OnTriggerEnter(Collider col)
    {
        Debug.Log("tt");
        if (col.gameObject.name == "Main Camera")
        {
            Debug.Log("tt");    
            FindObjectOfType<dialogueManager>().Start(dialogue);
            foreach (Collider c in GetComponents<Collider>())
            {
                Debug.Log("Collision");
                c.enabled = false;
            }
        }


    }
}
